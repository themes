
"A Duck's Claw" ("Clawrange") 0.3 - icon theme for Claws Mail 3.x, based on "Blaue Klaue" (German; blue claw). Created by Daniel Schneider in February and June 2008.

This theme is licenced under GPL v2 and Creative Commons BY-ND-SA.

linux@knetfeder.de
http://www.knetfeder.de/linux/index.php?entry=entry080206-120518
