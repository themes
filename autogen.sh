#!/bin/sh

./generate --replace --quiet \
    && aclocal \
    && automake --add-missing --foreign --copy \
    && autoconf \
    && ./configure --enable-maintainer-mode
