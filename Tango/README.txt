Theme name: Tango
Version: 1.1 (July 1, 2008)
Author: Ciprian Popovici <http://www.zuavra.net/>

This package contains a Claws Mail icon theme, tested agains Claws version
3.4.0+. This theme is made up entirely of PNG icons (the first Claws theme
to do this, to my knowledge.)

YOU NEED CLAWS MAIL VERSION 3.4.0 OR LATER IN ORDER TO BE ABLE TO USE THIS.

The icons in this package are an adaptation for the Claws Mail project of
the computer icons released by the Tango Project as version 0.8.1. As per
the Tango Project terms of use, this package is licensed under the CC-by-SA
license. Please see the LICENSE.txt file for details.

The Tango Project homepage: <http://tango.freedesktop.org/>

All the icons in this package are original Tango icons, original works
authored by me, or combinations of several Tango icons and/or original bits
by me.

The package contains two icons that vaguely resemble the Adobe PDF and PS
logos (mime_pdf.png and mime_ps.png, respectively). They were created from
scratch, not obtained by editing the original logos. It is not my intention
to infringe upon any trademarks owned by Adobe nor to create any confusion
regarding their products, but simply to provide means of loosely identifying
certain types of mail attachments.

END OF FILE
